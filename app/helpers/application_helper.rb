#Листинг 4.9: title_helper с примечаниями. app/helpers/application_helper.rb

module ApplicationHelper

  # Возвращает полный заголовок в зависимости от страницы. # Документ. комментарий
  def full_title(page_title = '')   # Определение метода, необязательный аргумент
    base_title = "June_24"  # Назначение переменной
    if page_title.empty?                              # Логическая проверка
      base_title                                      # Неявное возвращение
    else
      page_title + " | " + base_title                 # Соединение строк
    end
  end



end 
